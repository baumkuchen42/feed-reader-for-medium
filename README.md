# Feed Reader for Medium

An example application to show how intelligent agents can assist the user to fetch media from the web. Also motivated by wanting an easy way to read Medium articles. Because of how their feeds are structured, they are not really compatible with common RSS readers.

This feed reader scrapes the medium topics page for topics and then requests the respective RSS feeds. It also scrapes the article pages to retrieve their content as it is not included in Medium.com's RSS feed.

![image](/uploads/bb84fa2eb9698c85da84a6e2fd6ff0dd/image.png)

![image](/uploads/b00cc36a98ac12ac24e725faeec1d42f/image.png)

![image](/uploads/53155b6613e5207ef4a47176410a92cf/image.png)

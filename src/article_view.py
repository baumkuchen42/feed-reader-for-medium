# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from .feed_fetcher import get_feed, get_entry_by_index, get_content


@Gtk.Template(resource_path='/de/hszg/newsagent/article_view.ui')
class ArticleView(Gtk.ScrolledWindow):
	__gtype_name__ = 'ArticleView'
	title_label = Gtk.Template.Child()
	author_label = Gtk.Template.Child()
	article_label = Gtk.Template.Child()
	link_btn = Gtk.Template.Child()

	def __init__(self, topic, index, **kwargs):
		super().__init__(**kwargs)
		self.topic = topic
		self.index = index
		self.show_article()

	def show_article(self):
		entry = get_entry_by_index(self.topic, self.index)
		self.title_label.set_text(entry.get('title'))
		self.author_label.set_text(entry.get('author'))
		self.article_label.set_text(get_content(entry.get('link')))
		#print(get_content(entry.get('link')))
		self.link_btn.set_uri(entry.get('link'))

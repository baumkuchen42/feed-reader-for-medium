from urllib import request
import re

def download_topics_page():
	url = 'https://medium.com/topics'
	headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
		'Accept-Encoding': 'none',
		'Accept-Language': 'en-US,en;q=0.8',
		'Connection': 'keep-alive'}
	req = request.Request(url, headers=headers)

	page = request.urlopen(req)
	return page.read().decode('utf-8')

def clean_up(result):
	for entry in result:
		if len(entry.split()) > 1:
			result.remove(entry)
	return result

def find_topics():
	html_code = download_topics_page()
	for line in html_code.split('\n'):
		result = re.findall(r'href="https://medium.com/topic/(.+?)">', line)
		if len(result) > 0:
			return clean_up(result)

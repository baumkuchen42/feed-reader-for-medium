import feedparser
import json
import requests
from bs4 import BeautifulSoup

def get_feed(topic: str):
	'''
	an entry contains: [
		'title', 'title_detail', 'summary', 'summary_detail', 'links',
		'link', 'id', 'guidislink', 'tags', 'authors', 'author',
		'author_detail', 'published', 'published_parsed', 'updated', 'updated_parsed'
		]
	'''
	return feedparser.parse(f'https://medium.com/feed/topic/{topic}').get('entries')

def parse_summary(summary: str):
	parsed_summary = BeautifulSoup(summary, 'html.parser')
	return parsed_summary.get_text()

def get_entry_by_index(topic, index):
	return get_feed(topic)[index]

def get_content(link):
	content = BeautifulSoup(requests.get(link).content, 'html.parser')
	result = ''
	for paragraph in content.find_all('p'):
		result += paragraph.get_text() + '\n\n'
	return result

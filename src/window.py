# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from .topics_page import TopicsPage
from .articles_list import ArticlesList
from .article_view import ArticleView


@Gtk.Template(resource_path='/de/hszg/newsagent/window.ui')
class Window(Gtk.ApplicationWindow):
	__gtype_name__ = 'NewsagentTask2Window'
	main_stack = Gtk.Template.Child()
	back_btn = Gtk.Template.Child()

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.topics_page = TopicsPage()
		self.main_stack.add(self.topics_page)
		self.main_stack.set_visible_child(self.topics_page)
		self.connect_events()

	def connect_events(self):
		self.topics_page.topics_listbox.connect('row-activated', self.open_topic)
		self.back_btn.connect('clicked', self.go_back)

	def open_topic(self, listbox, row, data=None):
		topic = row.get_child().get_text()
		self.articles_list = ArticlesList(topic=topic)
		self.articles_list.articles_listbox.connect('row-activated', self.open_article)
		self.main_stack.add(self.articles_list)
		self.main_stack.set_visible_child(self.articles_list)
		self.back_btn.set_visible(True)

	def open_article(self, listbox, row, data=None):
		self.article_view = ArticleView(self.articles_list.topic, row.get_index())
		self.main_stack.add(self.article_view)
		self.main_stack.set_visible_child(self.article_view)

	def go_back(self, widget):
		if self.main_stack.get_visible_child() == self.topics_page:
			return
		elif self.main_stack.get_visible_child() == self.articles_list:
			self.main_stack.set_transition_type(Gtk.StackTransitionType.SLIDE_RIGHT)
			self.main_stack.set_visible_child(self.topics_page)
			self.back_btn.set_visible(False)
			self.main_stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)
		elif self.main_stack.get_visible_child() == self.article_view:
			self.main_stack.set_transition_type(Gtk.StackTransitionType.SLIDE_RIGHT)
			self.main_stack.set_visible_child(self.articles_list)
			self.main_stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT)

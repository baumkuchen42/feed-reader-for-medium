# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from .topics_finder import find_topics


@Gtk.Template(resource_path='/de/hszg/newsagent/topics_page.ui')
class TopicsPage(Gtk.ScrolledWindow):
	__gtype_name__ = 'TopicsPage'
	topics_listbox = Gtk.Template.Child()
	frame = Gtk.Template.Child()

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self.frame.set_size_request(500,0)
		self.load_topics()

	def load_topics(self):
		topic_list = find_topics()
		for topic in topic_list:
			row = Gtk.ListBoxRow()
			label = Gtk.Label()
			label.set_markup(f'<span size="13000">{topic.capitalize()}\n</span>')
			row.add(label)
			self.topics_listbox.add(row)
		self.topics_listbox.show_all()

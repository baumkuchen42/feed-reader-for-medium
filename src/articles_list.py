# window.py
#
# Copyright 2021 Uta
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk

from .feed_fetcher import get_feed, parse_summary


@Gtk.Template(resource_path='/de/hszg/newsagent/articles_list.ui')
class ArticlesList(Gtk.ScrolledWindow):
	__gtype_name__ = 'ArticlesList'
	articles_listbox = Gtk.Template.Child()

	def __init__(self, topic, **kwargs):
		super().__init__(**kwargs)
		self.topic = topic
		self.load_article_list()

	def load_article_list(self):
		entries = get_feed(self.topic)
		for entry in entries:
			row = self.assemble_article_row(entry)
			self.articles_listbox.add(row)
		self.articles_listbox.show_all()

	def assemble_article_row(self, entry):
		box = Gtk.VBox(homogeneous=False, spacing=5)

		title = entry.get('title')
		if title is not None:
			title_label = self.assemble_title_label(entry.get('title'))

		summary = entry.get('summary')
		if summary is not None:
			summary = parse_summary(summary)
			summary_label = self.assemble_summary_label(summary)

		if title_label and summary_label:
			box.add(title_label)
			box.add(summary_label)

		row = Gtk.ListBoxRow()
		row.add(box)
		return row

	def assemble_title_label(self, title):
		title_label = Gtk.Label()
		title_label.set_markup(f'<span weight="semibold" size="large">{title}</span>')
		title_label.set_line_wrap(True)
		return title_label

	def assemble_summary_label(self, summary):
		summary_label = Gtk.Label()
		summary_label.set_line_wrap(True)
		summary_label.set_text(summary+'\n')
		return summary_label
